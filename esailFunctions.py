import numpy as np
from scipy import constants
from decimal import Decimal

# Maybe a different object for the solar wind, that is passed to the Tether?

class SolarWind:
    """
    T_e (Solar wind e⁻ temperature) in eV.
    """
    
    def __init__(self):
        
        #self.n_0    =   7.3e6
        self.n_0    =   None    # (m⁻³) Undisturbed solar wind electron density.
        self.T_e    =   None    # J (solar wind electron temperature)
        self.v      =   None

    def update(self, n_0, T_e, v):
        
        self.n_0    =   n_0             # (m⁻³) Undisturbed solar wind electron density.
        self.T_e    =   T_e * 1.602e-19 # J (solar wind electron temperature)
        self.v      =   v               # (m/s) Solar wind velocity, I think.



class Tether:

    def __init__(self, r_w, V_0):
        
        self.r_w    =   r_w     # (m) Wire radius.
        self.V_0    =   V_0     # (V) Wire potential.

    def r_0(self, T_e, n_0):
        # From Janhunen 2007, equation 3.
        # Computes the distance at which the potential vanishes.
        
        num = constants.epsilon_0 * T_e
        den = n_0 * np.square(constants.e)  
        
        r_0 = 2 * np.sqrt(num / den)
        
        return r_0

    def electronDensity(self, solarWind, r):
        """
        From Janhunen 2007, equation 4.
        solarWind: SolarWind object
        """
        T_e = solarWind.T_e
        n_0 = solarWind.n_0  
        r_0 = self.r_0(T_e, n_0)
        
        den = 2 * T_e * np.square(1 + np.square(r / r_0)) * np.log(r_0 / self.r_w)
        
        result = n_0 * (1 + (constants.e * self.V_0) / den)
        
        return result
    
    def forcePerUnitL(self, solarWind):
        """
        From Janhunen 2007, equation 8.
        solarWind: SolarWind object.
        # Should K go in the solar wind object?
        """
        
        T_e = solarWind.T_e
        n_0 = solarWind.n_0 
        K   = 3.09          # Unitless? (Effective width of the potential structure?)
        v   = solarWind.v

        r_0 = self.r_0(T_e, n_0)

        num = K * constants.m_p * n_0 * np.square(v) * r_0 

        exp = ((constants.m_p * np.square(v)) / (constants.e * self.V_0)) * np.log(r_0 / self.r_w)

        den = np.sqrt(np.exp(exp) - 1)

        result = num / den

        return result


def potentialAroundWire(r, r_0, r_w, V_0):
    """
    From Janhunen 2007, equation 1.
    Untested.
    """
    
    return V_0 * (np.log(r_0 / r)) / (np.log(r_0 / r_w))

def shieldedPotential(r, r_w, T_e, V_0):
    """
    From Janhunen 2007, equation 1.
    Untested.
    T_e (Solar wind e⁻ temperature) in eV.
    V_0 (Wire potential) in V.
    r_w (Wire radius) in meters.
    """
    
    # Third time I calculate r_0. This is not good.

    n_0 = 7.3e6             # m^-3 (Undisturbed solar wind electron density)
    T_e = T_e * 1.602e-19   # J (solar wind electron temperature)

    r_0_numerator   =   constants.epsilon_0 * T_e
    
    r_0_denominator =   n_0 * np.square(constants.e)
    
    r_0 = 2 * np.sqrt(r_0_numerator / r_0_denominator)
    
    print(f"r_0 = {r_0}")

    result_numerator = np.log(1 + np.square(r_0 / r))
    
    result_denominator = np.log(1 + np.square(r_0 / r_w))

    result = V_0 * (result_numerator / result_denominator)

    return result



def forcePerUnitL(T_e, V_0, r_w):
    """
    From Janhunen 2007, equation 8.

    T_e (Solar wind e⁻ temperature) in eV.
    V_0 (Wire potential) in V.
    r_w (Wire radius) in meters.
    """

    K   = 3.09          # Unitless? (Effective width of the potential structure?)
    m_p = constants.m_p # Proton mass
    n_0 = 7.3e6         # m^-3 (Undisturbed solar wind electron density)
    v   = 400e3           # m·s^-1 
    T_e = T_e * 1.602e-19    # J (solar wind electron temperature)
    
    # r_0: distance at which the potential vanishes (V(r_0) == 0)
    
    r_0_numerator   =   constants.epsilon_0 * T_e
    
    r_0_denominator =   n_0 * np.square(constants.e)
    
    r_0 = 2 * np.sqrt(r_0_numerator / r_0_denominator)

    # Final formula, divided in small steps.
    
    force_numerator     =   K * m_p * n_0 * np.square(v) * r_0
    
    force_exponent      =   ((m_p * np.square(v)) / (constants.e * V_0)) * np.log(r_0 / r_w)

    force_radicand      =   np.exp(force_exponent) - 1

    force_denominator   =   np.sqrt(force_radicand)

    return(force_numerator / force_denominator)

def currentPerUnitL(V_0, r_w):
    """
    From Janhunen 2007, equation 9.
    Untested, I don't see results in the paper.
    V_0 (Wire potential) in V.
    """

    n_0 = 7.3e6         # m^-3 (Undisturbed solar wind electron density)

    root = np.sqrt( (2 * constants.e * V_0) / constants.m_e)

    result = constants.e * n_0 * root * 2 * r_w

    return result

