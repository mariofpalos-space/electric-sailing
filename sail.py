import numpy as np
import esailFunctions as esail
from scipy import constants
from decimal import Decimal
from matplotlib import pyplot as plt

"""
class Tether:
    
    def __init__(self, length, massPerL, charge):
        
        self.length         =   length #  m
        self.massPerL  =   massPerL # kg/m
        self.charge         =   charge

tether = Tether(length = 1, massPerL = .001, charge = 0)
"""

def scientific(value):
    return f"{Decimal(value):.2E}"


"""
print("6 ev:")
print(esail.forcePerUnitL(6, 30000, 10e-6)) 
print("12 ev:")
print(esail.forcePerUnitL(12, 30000, 10e-6)) 
print("24 ev:")
print(esail.forcePerUnitL(24, 30000, 10e-6)) 

"""

#print(esail.potentialAroundWire(V_0 = 10000, r = 1, r_0 = 1e6, r_w = 10e-6))
# Off by an order of magnitude.

#print(esail.shieldedPotential(r = 1, r_w = 10e-6, T_e = 12, V_0 = 10000))
# Off by an order of magnitude too.



wind = esail.SolarWind()
wind.update(n_0 = 7.3e6, T_e = 12, v = 4e5)

# I don't know the paper's V_0.
tether = esail.Tether(r_w = 10e-6, V_0 = 10000)

"""
# Plotting electron density
x = np.linspace(0, 50, 50)  # Or maybe 49?

eDensity = tether.electronDensity(wind, x) * 1e-6   # Converted to cm⁻³
plt.plot(x, eDensity, color = "red")

plt.xlabel("r (m)")
plt.ylabel("e⁻ density (cm⁻³)")

plt.show()
"""

# Plotting Force per unit length.
print("Force per L, 12 ev:")
print(esail.forcePerUnitL(T_e = 12, V_0 = 30000, r_w = 10e-6)) 
print(tether.forcePerUnitL(wind))
